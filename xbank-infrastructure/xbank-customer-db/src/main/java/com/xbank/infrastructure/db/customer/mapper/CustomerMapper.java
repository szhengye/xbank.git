package com.xbank.infrastructure.db.customer.mapper;

import com.xbank.infrastructure.db.customer.domain.Customer;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Entity com.xbank.infrastructure.db.customer.domain.Customer
 */
public interface CustomerMapper extends BaseMapper<Customer> {

}




