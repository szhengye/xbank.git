package com.xbank.infrastructure.db.payment.mapper;

import com.xbank.infrastructure.db.payment.domain.PaymentTranLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Entity com.xbank.infrastructure.db.payment.domain.PaymentTranLog
 */
public interface PaymentTranLogMapper extends BaseMapper<PaymentTranLog> {

}




