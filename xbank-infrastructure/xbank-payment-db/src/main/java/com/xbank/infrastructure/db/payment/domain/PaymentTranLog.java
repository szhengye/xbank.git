package com.xbank.infrastructure.db.payment.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;

/**
 * 缴费支付流水表
 * @TableName payment_tran_log
 */
@TableName(value ="payment_tran_log")
public class PaymentTranLog implements Serializable {
    /**
     * 
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 交易时间
     */
    private Date tranDatetime;

    /**
     * 交易码
     */
    private String tranCode;

    /**
     * 账号
     */
    private String accountId;

    /**
     * 交易金额
     */
    private Integer tranAmount;

    /**
     * 重试次数
     */
    private Integer retryCount;

    /**
     * 交易状态
     */
    private String tranStatus;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    /**
     * 
     */
    public Integer getId() {
        return id;
    }

    /**
     * 
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 交易时间
     */
    public Date getTranDatetime() {
        return tranDatetime;
    }

    /**
     * 交易时间
     */
    public void setTranDatetime(Date tranDatetime) {
        this.tranDatetime = tranDatetime;
    }

    /**
     * 交易码
     */
    public String getTranCode() {
        return tranCode;
    }

    /**
     * 交易码
     */
    public void setTranCode(String tranCode) {
        this.tranCode = tranCode;
    }

    /**
     * 账号
     */
    public String getAccountId() {
        return accountId;
    }

    /**
     * 账号
     */
    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    /**
     * 交易金额
     */
    public Integer getTranAmount() {
        return tranAmount;
    }

    /**
     * 交易金额
     */
    public void setTranAmount(Integer tranAmount) {
        this.tranAmount = tranAmount;
    }

    /**
     * 重试次数
     */
    public Integer getRetryCount() {
        return retryCount;
    }

    /**
     * 重试次数
     */
    public void setRetryCount(Integer retryCount) {
        this.retryCount = retryCount;
    }

    /**
     * 交易状态
     */
    public String getTranStatus() {
        return tranStatus;
    }

    /**
     * 交易状态
     */
    public void setTranStatus(String tranStatus) {
        this.tranStatus = tranStatus;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        PaymentTranLog other = (PaymentTranLog) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getTranDatetime() == null ? other.getTranDatetime() == null : this.getTranDatetime().equals(other.getTranDatetime()))
            && (this.getTranCode() == null ? other.getTranCode() == null : this.getTranCode().equals(other.getTranCode()))
            && (this.getAccountId() == null ? other.getAccountId() == null : this.getAccountId().equals(other.getAccountId()))
            && (this.getTranAmount() == null ? other.getTranAmount() == null : this.getTranAmount().equals(other.getTranAmount()))
            && (this.getRetryCount() == null ? other.getRetryCount() == null : this.getRetryCount().equals(other.getRetryCount()))
            && (this.getTranStatus() == null ? other.getTranStatus() == null : this.getTranStatus().equals(other.getTranStatus()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getTranDatetime() == null) ? 0 : getTranDatetime().hashCode());
        result = prime * result + ((getTranCode() == null) ? 0 : getTranCode().hashCode());
        result = prime * result + ((getAccountId() == null) ? 0 : getAccountId().hashCode());
        result = prime * result + ((getTranAmount() == null) ? 0 : getTranAmount().hashCode());
        result = prime * result + ((getRetryCount() == null) ? 0 : getRetryCount().hashCode());
        result = prime * result + ((getTranStatus() == null) ? 0 : getTranStatus().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", tranDatetime=").append(tranDatetime);
        sb.append(", tranCode=").append(tranCode);
        sb.append(", accountId=").append(accountId);
        sb.append(", tranAmount=").append(tranAmount);
        sb.append(", retryCount=").append(retryCount);
        sb.append(", tranStatus=").append(tranStatus);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}