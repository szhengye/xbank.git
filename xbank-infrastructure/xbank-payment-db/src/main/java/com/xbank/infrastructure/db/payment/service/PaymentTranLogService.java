package com.xbank.infrastructure.db.payment.service;

import com.xbank.infrastructure.db.payment.domain.PaymentTranLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *
 */
public interface PaymentTranLogService extends IService<PaymentTranLog> {

}
