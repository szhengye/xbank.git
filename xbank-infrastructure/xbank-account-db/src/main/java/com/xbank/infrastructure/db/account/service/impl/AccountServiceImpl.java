package com.xbank.infrastructure.db.account.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xbank.infrastructure.db.account.domain.Account;
import com.xbank.infrastructure.db.account.service.AccountService;
import com.xbank.infrastructure.db.account.mapper.AccountMapper;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class AccountServiceImpl extends ServiceImpl<AccountMapper, Account>
    implements AccountService{

}




