package com.xbank.infrastructure.db.account.service;

import com.xbank.infrastructure.db.account.domain.Account;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *
 */
public interface AccountService extends IService<Account> {

}
