package com.xbank.client.app.dto;

import com.xbank.infrastructure.db.account.domain.Account;
import com.xbank.infrastructure.db.customer.domain.Customer;
import lombok.Data;

import java.util.List;

@Data
public class CustomerAndAccountList {
    private Customer customer;
    private List<Account> accountList;
}
