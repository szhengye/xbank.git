package com.xbank.client.sink.account.api;

import com.xbank.infrastructure.db.account.domain.Account;

import java.util.List;

public interface AccountSinkInterface {
    public List<Account> getAccountListByCustomerId(String customerId);
    public Account payout(String accountId,long amount);
    public Account payoutCompensation(String accountId,long amount);
}
