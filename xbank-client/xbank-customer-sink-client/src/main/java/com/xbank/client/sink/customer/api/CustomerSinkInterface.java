package com.xbank.client.sink.customer.api;

import com.xbank.infrastructure.db.customer.domain.Customer;

public interface CustomerSinkInterface {
    public Customer getCustomer(String customerId);
}
