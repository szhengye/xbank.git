package com.xbank.sink.customer.service;

import com.xbank.client.sink.customer.api.CustomerSinkInterface;
import com.xbank.infrastructure.db.customer.service.CustomerService;
import com.xbank.infrastructure.db.customer.domain.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerSinkService implements CustomerSinkInterface {
    @Autowired
    private CustomerService custmerService;

    @Override
    public Customer getCustomer(String customerId) {
        Customer customer = this.custmerService.getById(customerId);
        return customer;
    }
}
