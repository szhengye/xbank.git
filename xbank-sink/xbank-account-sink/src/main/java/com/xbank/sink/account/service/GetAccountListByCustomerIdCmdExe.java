package com.xbank.sink.account.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bizmda.bizsip.sink.cmdexe.AbstractBeanCmdExe;
import com.xbank.infrastructure.db.account.domain.Account;
import com.xbank.infrastructure.db.account.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GetAccountListByCustomerIdCmdExe extends AbstractBeanCmdExe {
    @Autowired
    private AccountService accountService;

    public List<Account> getAccountListByCustomerId(String customerId) {
        QueryWrapper<Account> queryWrapper=new QueryWrapper();
        queryWrapper.eq("customer_id",customerId);
        return this.accountService.list(queryWrapper);
    }
}
