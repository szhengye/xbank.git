package com.xbank.sink.account.service;

import com.xbank.client.sink.account.api.AccountSinkInterface;
import com.xbank.infrastructure.db.account.domain.Account;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class AccountSinkService implements AccountSinkInterface {
    @Autowired
    private PayoutCmdExe payoutCmdExe;
    @Autowired
    private  PayoutCompensationCmdExe payoutCompensationCmdExe;
    @Autowired
    private GetAccountListByCustomerIdCmdExe getAccountListByCustomerIdCmdExe;

    @Override
    public List<Account> getAccountListByCustomerId(String customerId) {
        return this.getAccountListByCustomerIdCmdExe.getAccountListByCustomerId(customerId);
    }

    @Override
    public Account payout(String accountId, long amount) {
        return this.payoutCmdExe.payout(accountId,amount);
    }

    @Override
    public Account payoutCompensation(String accountId, long amount) {
        return this.payoutCompensationCmdExe.payoutCompensation(accountId,amount);
    }
}
